
# Single Status Authorisation Plugin for MSM

This plugin allows you to restrict which MSM users are permitted to move a request from a nominated status to the next status in the workflow.

## Compatible Versions

| Plugin  | MSM                    |
|---------|------------------------|
| 1.0.0   | 14.3.0, 14.4.0, 14.5.0 |
| 1.0.1   | 15.1+                  |

## Installation

Please see your MSM documentation for information on how to install plugins.

Once the plugin has been installed you will need to configure the following settings within the plugin page:

+ *Role* : The name of a role that is assuming responsibility for single status authorisation. There are no options within the role for this plugin.
+ *Status* : The name of the a status which requires restriction.

## Usage

The plugin will cross reference the users **roles** agaisnt the role specified on the plugin page and the request status of the loaded request against the **status** specified on the plugin page and any person not holding the role where the status matches are not permitted to move the request status on.

## Limitations

The plugin will not restrict users changing a request status when updated via Bulk Update, Related Request and other associated rules.

## Contributing

We welcome all feedback including feature requests and bug reports. Please raise these as issues on GitHub. If you would like to contribute to the project please fork the repository and issue a pull request.