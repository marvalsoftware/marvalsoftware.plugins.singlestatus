﻿<%@ WebHandler Language="C#" Class="ApiHandler" %>

using System.Net;
using System.Web;
using Newtonsoft.Json.Linq;
using MarvalSoftware.UI.WebUI.ServiceDesk.RFP.Plugins;

/// <summary>
/// ApiHandler
/// </summary>
public class ApiHandler : PluginHandler
{
    // properties
    private string SingleRole
    {
        get
        {
            return GlobalSettings["Role"];
        }
    }

    private string SingleStatus
    {
        get
        {
            return GlobalSettings["Status"];
        }
    }

    // fields
    private int MsmRequestNo;

    /// <summary>
    /// Handle Request
    /// </summary>
    public override void HandleRequest(HttpContext context)
    {
        this.ProcessParamaters(context.Request);

        var action = context.Request.QueryString["action"];
        this.RouteRequest(action, context);
    }

    public override bool IsReusable
    {
        get { return false; }
    }

    /// <summary>
    /// Get Paramaters from QueryString
    /// </summary>
    private void ProcessParamaters(HttpRequest httpRequest)
    {
        int.TryParse(httpRequest.Params["requestNumber"], out this.MsmRequestNo);
    }

    /// <summary>
    /// Route Request via Action
    /// </summary>
    private void RouteRequest(string action, HttpContext context)
    {
        switch (action)
        {
            case "PreRequisiteCheck":
                context.Response.Write(this.PreRequisiteCheck());
                break;
            case "CheckUserPermisison":
                context.Response.Write(this.CheckUserPermisison());
                break;
        }
    }

    /// <summary>
    /// Check and return missing plugin settings
    /// </summary>
    /// <returns>Json Object containing any settings that failed the check</returns>
    private JObject PreRequisiteCheck()
    {
        var preReqs = new JObject();

        if (string.IsNullOrWhiteSpace(this.SingleRole))
        {
            preReqs.Add("singleRole", false);
        }
        if (string.IsNullOrWhiteSpace(this.SingleStatus))
        {
            preReqs.Add("singleStatus", false);
        }

        return preReqs;
    }

    /// <summary>
    /// Check whether the current user has the required role
    /// </summary>
    /// <returns>Json Object containing any settings that failed the check</returns>
    private JObject CheckUserPermisison()
    {
        var preReqs = new JObject();

        if (!string.IsNullOrEmpty(this.SingleRole))
        {
            if (MarvalSoftware.Security.User.CurrentUser.Roles.IsInRole(this.SingleRole))
            {
                preReqs.Add("role", this.SingleRole);
            }
            else
            {
                preReqs.Add("status", this.SingleStatus);
                preReqs.Add("heading", "Single Status Authorisation");
                preReqs.Add("message", "You do not have permission to change the request status.");
            }
        }

        return preReqs;
    }
}